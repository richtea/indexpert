import mongodb from 'mongodb';
import { url } from './helpers';

export const getServerStatus = (connection) => {
  const MongoClient = mongodb.MongoClient;
  return MongoClient.connect(url(connection))
    .then(db => db.command({ serverStatus: 1 })
      .then((status) => {
        db.close();
        return status;
      }));
};

export default getServerStatus;
