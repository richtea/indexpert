import * as formatters from '../helpers/format';
import { getNestedProperty, setNestedProperty } from '../helpers/obj';

export const findStages = (fieldName, obj) => {
  if (!obj) {
    return [];
  }

  return Object.keys(obj).reduce((prev, curr) => {
    if (typeof obj[curr] === 'object') {
      const st = findStages(fieldName, obj[curr]);

      if (st.length > 0) {
        prev = [...prev, ...st];
      }
    } else if (curr === fieldName) {
      prev = [...prev, obj[curr]];
    }

    return prev;
  }, []);
};

export const getCollectionName = profile => profile.ns.substring(profile.ns.indexOf('.') + 1);

export const queryIsSlow = (profile, slowms) => profile.millis >= slowms;

export const queryIsNearlySlow = (profile, slowms) => profile.millis >= (slowms * 0.75);

export const getIndexNames = profile => findStages('indexName', profile.execStats);

export const getStageNames = profile => findStages('stage', profile.execStats);

export const hasStage = (stageName, profile) => {
  if (profile.stages) {
    return profile.stages.includes(stageName);
  }

  return getStageNames(profile).includes(stageName);
};

export const usedIndex = (profile) => {
  if (profile.indexes) {
    return profile.indexes.length > 0;
  }

  return getIndexNames(profile).length > 0;
};

export const fields = {
  'computed.raw': {
    compute: profile => JSON.stringify(profile, null, '\t'),
  },
  'computed.collection': {
    compute: getCollectionName,
    default: true,
  },
  'computed.stages': {
    compute: getStageNames,
    format: formatters.formatArray,
    default: true,
  },
  'computed.indexes': {
    compute: getIndexNames,
    format: formatters.formatArray,
    default: true,
  },
  'computed.isSlow': {
    compute: queryIsSlow,
    warn: (v) => {
      if (v) {
        return 1;
      }
      return 0;
    },
  },
  'computed.isNearlySlow': {
    warn: (v) => {
      if (v) {
        return 1;
      }
      return 0;
    },
    compute: queryIsNearlySlow,
  },
  'computed.usedSort': {
    compute: profile => hasStage('SORT', profile),
    warn: (v) => {
      if (v === false) {
        return 1;
      }
      return 0;
    },
  },
  'computed.usedCollscan': {
    compute: profile => hasStage('COLLSCAN', profile),
    warn: (v) => {
      if (v) {
        return 1;
      }
      return 0;
    },
  },
  'computed.usedIndex': { compute: usedIndex },
  op: { default: true },
  ns: {},
  cursorid: {},
  keysExamined: { default: true },
  docsExamined: {},
  cursorExhausted: {},
  moved: {},
  hasSortStage: {
    default: true,
    warn: (v) => {
      if (v !== true) {
        return 1;
      }
      return 0;
    },
  },
  ndeleted: {},
  ninserted: {},
  nMatched: {},
  nModified: {},
  upsert: {},
  fromMultiPlanner: {},
  keysInserted: {},
  keysDeleted: {},
  writeConflicts: {},
  nreturned: { default: true },
  responseLength: {
    format: formatters.formatBytes,
    default: true,
  },
  protocol: {},
  millis: {
    default: true,
    warn: (v) => {
      if (v >= 200) {
        return 1;
      }
      return 0;
    },
  },
  planSummary: {},
  ts: {
    format: formatters.formatDate,
    default: true,
  },
  client: {},
  appName: {},
  user: {},
};

export const addComputedFields = (profile, slowms) => {
  if (!profile) {
    return undefined;
  }

  const ret = profile;

  Object.keys(fields).forEach((key) => {
    if (fields[key].compute) {
      setNestedProperty(ret, key, fields[key].compute(ret, slowms));
    }
  });

  return ret;
};

export const formatProperty = (obj, key) => {
  const val = getNestedProperty(obj, key);
  const knownKey = fields[key];

  if (knownKey && knownKey.format) {
    return knownKey.format(val);
  }

  if (val === undefined || val === null || val === []) {
    return '';
  }

  return val;
};

export const getPropertyWarning = (obj, key) => {
  const val = getNestedProperty(obj, key);
  const knownKey = fields[key];

  if (knownKey && knownKey.warn) {
    return knownKey.warn(val);
  }

  return 0;
};

export const getDefaultFields = () =>
  Object.keys(fields).filter(key => fields[key].default === true);

export default fields;
