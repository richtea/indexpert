import mongodb from 'mongodb';
import { url } from './helpers';

export const list = (connection) => {
  const MongoClient = mongodb.MongoClient;
  return MongoClient.connect(url(connection))
    .then((db) => {
      const adminDb = db.admin();
      return adminDb.listDatabases()
        .then((dbs) => {
          db.close();
          return dbs;
        });
    });
};

export const getProfilingLevel = (connection, dbname) => {
  const MongoClient = mongodb.MongoClient;
  return MongoClient.connect(url(connection, dbname))
    .then(db => db.command({ profile: -1 })
      .then((doc) => {
        db.close();
        return {
          ...doc,
          is: doc.was,
        };
      }));
};

export const setProfilingLevel = (connection, dbname, level) => {
  const MongoClient = mongodb.MongoClient;
  return MongoClient.connect(url(connection, dbname))
    .then(db => db.command({ profile: Number(level) })
      .then((doc) => {
        db.close();
        return {
          ...doc,
          is: Number(level),
        };
      }));
};

export default list;
