import mongodb from 'mongodb';
import { url } from './helpers';

export const testConnection = (connection) => {
  const MongoClient = mongodb.MongoClient;
  return MongoClient.connect(url(connection))
    .then(db => db.close());
};

export default testConnection;
