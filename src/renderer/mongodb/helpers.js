
export const url = ({ host, port, username, password, database, connectionString }, db) => {
  if (connectionString) {
    if (db) {
      const split = connectionString.split('/?');
      return `${split[0]}/${db}?${split[1]}`;
    }
    return connectionString;
  }
  let p = port;
  let u = '';
  if (!port) {
    p = '27017';
  }

  if (username && password) {
    u = `${username}:${password}@`;
  }

  let url = `mongodb://${u}${host}:${p}`;

  if (database) {
    url = `${url}/${database}`;
  } else if (db) {
    url = `${url}/${db}`;
  }

  return url;
};

export default url;
