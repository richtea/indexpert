import mongodb from 'mongodb';
import { url } from './helpers';

const buildProfilerQuery = (dbname, filters) => {
  const ret = { ns: { $ne: `${dbname}.system.profile` } };
  if (!filters) {
    return ret;
  }

  if (filters.col) {
    ret.ns = `${dbname}.${filters.col}`;
  }

  if (filters.op) {
    ret.op = filters.op;
  }

  if (filters.ms) {
    ret.millis = { $gte: Number(filters.ms) };
  }

  if (filters.client) {
    if (filters.resolvedClient) {
      ret.client = filters.resolvedClient;
    } else {
      ret.client = filters.client;
    }
  }

  if (filters.hasSortStage !== null) {
    ret.hasSortStage = filters.hasSortStage === true ? true : { $ne: true };
  }

  return ret;
};

export const findStages = function reducer(fieldName, obj) {
  if (!obj) {
    return [];
  }

  return Object.keys(obj).reduce((prev, curr) => {
    if (typeof obj[curr] === 'object') {
      const st = findStages(fieldName, obj[curr]);
      if (st.length > 0) {
        prev = [...prev, ...st];
      }
    } else if (curr === fieldName) {
      prev = [...prev, obj[curr]];
    }
    return prev;
  }, []);
};

export const listCollections = (connection, dbname) => {
  const MongoClient = mongodb.MongoClient;

  return MongoClient.connect(url(connection, dbname))
    .then(db => db.listCollections().toArray()
      .then((data) => {
        db.close();
        return data;
      }));
};

export const list = (connection, dbname, filters) => {
  const MongoClient = mongodb.MongoClient;

  return MongoClient.connect(url(connection, dbname))
    .then((db) => {
      const col = db.collection('system.profile');
      const q = buildProfilerQuery(dbname, filters);

      return col.find(q).sort({ ts: -1 }).toArray()
        .then((data) => {
          db.close();
          return data;
        });
    });
};

export default list;
