import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage').default,
    },
    {
      path: '/profile/:dbname',
      name: 'profile-page',
      component: require('@/components/ProfilePage').default,
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});
