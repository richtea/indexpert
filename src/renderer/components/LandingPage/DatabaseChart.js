import { Doughnut } from 'vue-chartjs';
import { formatBytes } from '../../helpers/format';

const options = {
  responsive: true,
  maintainAspectRatio: false,
  legend: { display: false },
  tooltips: {
    callbacks: {
      label(item, data) {
        return `${data.labels[item.index]}: ${formatBytes(data.datasets[0].data[item.index])}`;
      },
    },
  },
  animation: { animateScale: true },
  cutoutPercentage: 60,
};

export default ({
  extends: Doughnut,
  props: ['data'],
  mounted() {
    this.renderChart(this.data, options);
  },
  watch: {
    data() {
      // eslint-disable-next-line no-underscore-dangle
      this._chart.destroy();
      this.renderChart(this.data, options);
    },
  },
});
