import { Line } from 'vue-chartjs';
import colors from 'vuetify/es5/util/colors';

const options = {
  responsive: true,
  maintainAspectRatio: false,
  legend: { display: false },
  tooltips: {
    mode: 'label',
    intersect: false,
  },
  scales: {
    xAxes: [{
      type: 'time',
      display: true,
      ticks: {
        autoSkip: true,
        maxTicksLimit: 20,
      },
      time: {
        unit: 'minute',
        displayFormats: {
          minute: 'DD/MM HH:mm',
        },
      },
    }],
    yAxes: [{
      display: false,
    }],
  },
  fill: false,
};

export default ({
  extends: Line,
  mounted() {
    this.renderChart(this.data, options);
  },
  computed: {
    data() {
      if (this.$store.state.profile.data.length === 0) {
        return ({ datasets: [{ data: [] }] });
      }
      const datasets = {};
      const ticks = 250;
      const dates = this.$store.state.profile.data.map(d => new Date(d.ts));
      const dateRange = dates[0].getTime() - dates[dates.length - 1].getTime();
      const dateTick = dateRange / ticks;
      const colorsArray = Object.values(colors);
      const setupDataset = (name) => {
        const color = colorsArray[Math.floor(Math.random() * colorsArray.length)].base;

        datasets[name] = {
          label: name,
          fill: false,
          backgroundColor: color,
          borderColor: color,
          pointRadius: 0,
          data: new Array(ticks + 1),
        };

        for (let i = 0; i < datasets[name].data.length; i += 1) {
          datasets[name].data[i] = {
            x: new Date(dates[dates.length - 1].getTime() + (dateTick * i)),
            y: 0,
          };
        }
      };

      for (let d = 0; d < dates.length; d += 1) {
        const prof = this.$store.state.profile.data[d];
        const date = dates[d];
        const loc = Math.floor((date.getTime() - dates[dates.length - 1].getTime()) / dateTick);

        if (!datasets[prof.op]) {
          setupDataset(prof.op);
        }

        datasets[prof.op].data[loc].y += 1;
      }

      return ({
        datasets: Object.values(datasets),
      });
    },
  },
  watch: {
    data() {
      // eslint-disable-next-line no-underscore-dangle
      this._chart.destroy();
      this.renderChart(this.data, options);
    },
  },
});
