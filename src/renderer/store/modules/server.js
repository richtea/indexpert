import { getServerStatus } from '../../mongodb/server';

const state = {
  status: null,
  loading: false,
  success: false,
  error: null,
};

const mutations = {
  GET_STATUS_START(state) {
    state.status = null;
    state.loading = true;
    state.success = false;
    state.error = null;
  },
  GET_STATUS_SUCCESS(state, status) {
    state.status = status;
    state.loading = false;
    state.success = true;
    state.error = null;
  },
  GET_STATUS_ERROR(state, err) {
    state.status = null;
    state.loading = false;
    state.success = false;
    state.error = err.message;
  },
};

const actions = {
  getStatus({ commit, rootState }) {
    commit('GET_STATUS_START');
    getServerStatus(rootState.settings.connection.current)
      .then(status => commit('GET_STATUS_SUCCESS', status))
      .catch(err => commit('GET_STATUS_ERROR', err));
  },
};

export default {
  state,
  mutations,
  actions,
};
