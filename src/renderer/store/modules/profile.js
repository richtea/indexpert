import debounce from 'debounce';
import dns from 'dns';
import { list, listCollections as listCols } from '../../mongodb/profileQuery';
import { addComputedFields, getDefaultFields } from '../../mongodb/profileOutput';

const state = {
  filters: {
    selected: {
      col: null,
      op: null,
      client: null,
      resolvedClient: null,
      ms: null,
      hasSortStage: null,
    },
    collections: [],
    loading: false,
    success: false,
    error: null,
  },
  data: [],
  loading: false,
  success: false,
  error: null,
  columns: getDefaultFields(),
};

const mutations = {
  SET_COLLECTION_FILTER(state, collection) {
    state.filters.selected.col = collection;
  },
  SET_OP_FILTER(state, op) {
    state.filters.selected.op = op;
  },
  SET_CLIENT_FILTER(state, { client, resolvedClient }) {
    state.filters.selected.client = client;
    state.filters.selected.resolvedClient = resolvedClient;
  },
  SET_MS_FILTER(state, ms) {
    state.filters.selected.ms = ms;
  },
  SET_HAS_SORT_STAGE_FILTER(state, hasSortStage) {
    state.filters.selected.hasSortStage = hasSortStage;
  },
  SET_COLUMNS(state, columns) {
    state.columns = columns;
  },
  GET_PROFILE_START(state) {
    state.loading = true;
    state.success = false;
    state.error = null;
  },
  GET_PROFILE_SUCCESS(state, data) {
    state.data = data;
    state.loading = false;
    state.success = true;
    state.error = null;
  },
  GET_PROFILE_ERROR(state, err) {
    state.loading = false;
    state.success = false;
    state.error = err;
  },
  LIST_COLLECTIONS_START(state) {
    state.filters.loading = true;
    state.filters.success = false;
    state.filters.error = null;
  },
  LIST_COLLECTIONS_SUCCESS(state, data) {
    state.filters.collections = data.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });
    state.filters.selected.col = null;
    state.filters.loading = false;
    state.filters.success = true;
    state.filters.error = null;
  },
  LIST_COLLECTIONS_ERROR(state, err) {
    state.filters.loading = false;
    state.filters.success = false;
    state.filters.error = err.message;
  },
};

const actions = {
  updateCollectionFilter({ commit }, col) {
    commit('SET_COLLECTION_FILTER', col);
  },
  updateOpFilter({ commit }, op) {
    commit('SET_OP_FILTER', op);
  },
  updateMsFilter: debounce(({ commit }, ms) => {
    commit('SET_MS_FILTER', ms);
  }, 1000),
  updateHasSortStageFilter({ commit }, hasSortStage) {
    commit('SET_HAS_SORT_STAGE_FILTER', hasSortStage);
  },
  updateProfileColumns({ commit }, cols) {
    commit('SET_COLUMNS', cols);
  },
  updateClientFilter: debounce(({ commit }, client) => {
    if (client.match(/[a-z]/i)) {
      dns.lookup(client, (err, address) => {
        if (err) {
          commit('SET_CLIENT_FILTER', { client });
        } else {
          commit('SET_CLIENT_FILTER', { client, resolvedClient: address });
        }
      });
    } else {
      commit('SET_CLIENT_FILTER', { client });
    }
  }, 1000),
  listCollections({ commit, rootState }, db) {
    commit('LIST_COLLECTIONS_START');
    listCols(rootState.settings.connection.current, db)
      .then(data => commit('LIST_COLLECTIONS_SUCCESS', data))
      .catch(err => commit('LIST_COLLECTIONS_ERROR', err));
  },
  getProfileData({ commit, rootState }, { db, filter }) {
    const slowms = rootState.dbs.levels[db].slowms;
    commit('GET_PROFILE_START');
    list(rootState.settings.connection.current, db, filter)
      .then((data) => {
        const processedData = data.map(d => addComputedFields(d, slowms));
        commit('GET_PROFILE_SUCCESS', processedData);
      })
      .catch(err => commit('GET_PROFILE_ERROR', err));
  },
};

export default {
  state,
  mutations,
  actions,
};
