import { list, getProfilingLevel, setProfilingLevel } from '../../mongodb/dbs';

const state = {
  dbs: [],
  levels: {},
  loading: false,
  success: false,
  error: null,
};

const mutations = {
  SET_PROFILE_LEVEL(state, { db, levelInfo }) {
    state.dbs = state.dbs.map((database) => {
      if (database.name === db.name) {
        return {
          ...database,
          profilingLevel: `${levelInfo.is}`,
          profilingSlowMs: levelInfo.slowms,
        };
      }
      return database;
    });
    state.levels[db.name] = levelInfo;
  },
  LIST_DBS_START(state) {
    state.loading = true;
    state.success = false;
    state.error = null;
  },
  LIST_DBS_SUCCESS(state, dbs) {
    state.dbs = dbs.databases;
    state.loading = false;
    state.success = true;
    state.error = null;
  },
  LIST_DBS_ERROR(state, err) {
    state.loading = false;
    state.success = false;
    state.error = err.message;
  },
};

const actions = {
  listDatabases({ commit, rootState }) {
    commit('LIST_DBS_START');
    list(rootState.settings.connection.current)
      .then((dbs) => {
        commit('LIST_DBS_SUCCESS', dbs);
        return dbs.databases.forEach(db =>
          getProfilingLevel(rootState.settings.connection.current, db.name)
            .then(levelInfo => commit('SET_PROFILE_LEVEL', { db, levelInfo })));
      })
      .catch(err => commit('LIST_DBS_ERROR', err));
  },
  setProfilingLevel({ commit, rootState }, { db, level }) {
    setProfilingLevel(rootState.settings.connection.current, db.name, level)
      .then(levelInfo => commit('SET_PROFILE_LEVEL', { db, levelInfo }))
      .catch(err => commit('LIST_DBS_ERROR', err));
  },
};

export default {
  state,
  mutations,
  actions,
};
