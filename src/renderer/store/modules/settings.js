import { testConnection } from '../../mongodb/connection';

const saveRecentConnections = (connections) => {
  localStorage.setItem('recentconnections', JSON.stringify(connections));
};

const loadRecentConnections = () => {
  const ret = localStorage.getItem('recentconnections');
  if (ret !== null) {
    return JSON.parse(ret);
  }
  return [];
};

const existingConnectionIndex = (newConn, existingConns) =>
  existingConns.findIndex(conn => JSON.stringify(newConn) === JSON.stringify(conn));

const state = {
  showDialog: true,
  darkTheme: localStorage.getItem('darkTheme') !== 'false',
  connection: {
    current: {
      name: '',
      host: '',
      port: '',
      user: '',
      password: '',
      database: '',
      connectionString: '',
    },
    form: {
      name: '',
      host: '',
      port: '',
      user: '',
      password: '',
      database: '',
      connectionString: '',
    },
    test: {
      loading: false,
      success: false,
      error: null,
    },
    recent: loadRecentConnections(),
  },
};

const mutations = {
  SHOW_DIALOG(state) {
    state.showDialog = true;
  },
  HIDE_DIALOG(state) {
    state.showDialog = false;
  },
  SET_FORM(state, form) {
    state.connection.form = { ...form };
  },
  SET_FORM_HOST(state, host) {
    state.connection.form.host = host;
  },
  SET_FORM_PORT(state, port) {
    state.connection.form.port = port;
  },
  SET_FORM_DATABASE(state, db) {
    state.connection.form.database = db;
  },
  SET_FORM_USERNAME(state, username) {
    state.connection.form.username = username;
  },
  SET_FORM_PASSWORD(state, password) {
    state.connection.form.password = password;
  },
  SET_FORM_CONNECTION_STRING(state, connectionString) {
    state.connection.form.connectionString = connectionString;
  },
  SET_DARK_THEME(state, dark) {
    state.darkTheme = dark;
  },
  TEST_CONNECTION_CLEAR(state) {
    state.connection.test = {
      loading: false,
      success: false,
      error: null,
    };
  },
  SAVE_CONNECTION_START(state) {
    state.connection.test = {
      loading: true,
      success: false,
      error: null,
    };
  },
  SAVE_CONNECTION_SUCCESS(state, connection) {
    state.connection.current = connection;
    state.connection.test = {
      loading: false,
      success: true,
      error: null,
    };
  },
  SAVE_CONNECTION_ERROR(state, err) {
    state.connection.test = {
      loading: false,
      success: false,
      error: err.message,
    };
  },
  ADD_RECENT_CONNECTION(state, { conn, index }) {
    if (index === -1) {
      state.connection.recent = [
        { ...conn },
        ...state.connection.recent,
      ];
    } else {
      state.connection.recent = [...state.connection.recent];
      state.connection.recent[index] = { ...conn };
    }
  },
};

const actions = {
  updateFormHost({ commit }, host) {
    commit('SET_FORM_HOST', host);
    commit('TEST_CONNECTION_CLEAR');
  },
  updateFormPort({ commit }, port) {
    commit('SET_FORM_PORT', port);
    commit('TEST_CONNECTION_CLEAR');
  },
  updateFormDatabase({ commit }, host) {
    commit('SET_FORM_DATABASE', host);
    commit('TEST_CONNECTION_CLEAR');
  },
  updateFormUserName({ commit }, host) {
    commit('SET_FORM_USERNAME', host);
    commit('TEST_CONNECTION_CLEAR');
  },
  updateFormPassword({ commit }, port) {
    commit('SET_FORM_PASSWORD', port);
    commit('TEST_CONNECTION_CLEAR');
  },
  updateFormConnectionString({ commit }, port) {
    commit('SET_FORM_CONNECTION_STRING', port);
    commit('TEST_CONNECTION_CLEAR');
  },
  updateDarkTheme({ commit }, dark) {
    commit('SET_DARK_THEME', dark);
    localStorage.setItem('darkTheme', dark);
  },
  selectRecentConnection({ commit }, conn) {
    commit('SET_FORM', conn);
  },
  saveConnection({ commit, state }) {
    commit('SAVE_CONNECTION_START');
    return testConnection(state.connection.form)
      .then(() => {
        commit('SAVE_CONNECTION_SUCCESS', state.connection.form);
        const index = existingConnectionIndex(state.connection.form, state.connection.recent);
        commit('ADD_RECENT_CONNECTION', { conn: state.connection.form, index });
        saveRecentConnections(state.connection.recent);
        commit('HIDE_DIALOG');
      })
      .catch(err => commit('SAVE_CONNECTION_ERROR', err));
  },
  showDialog({ commit }) {
    commit('SHOW_DIALOG');
  },
  hideDialog({ commit }) {
    commit('HIDE_DIALOG');
  },
};

export default {
  state,
  mutations,
  actions,
};
