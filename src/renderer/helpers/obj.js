export const getNestedProperty = (obj, key) => {
  const keyBits = key.split('.');

  if (obj === undefined || keyBits.length === 0) {
    return undefined;
  }

  const nestedObj = obj[keyBits[0]];

  if (keyBits.length > 1) {
    return getNestedProperty(nestedObj, key.substring(keyBits[0].length + 1));
  }

  return nestedObj;
};

export const setNestedProperty = (obj, key, val) => {
  const keyBits = key.split('.');

  if (obj === undefined || keyBits.length === 0) {
    return;
  }

  if (!obj[keyBits[0]]) {
    obj[keyBits[0]] = {};
  }

  if (keyBits.length > 1) {
    setNestedProperty(obj[keyBits[0]], key.substring(keyBits[0].length + 1), val);
    return;
  }

  obj[keyBits[0]] = val;
};

export default getNestedProperty;
