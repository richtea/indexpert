import Vue from 'vue';
import Vuetify from 'vuetify';
import VueHighlightJS from 'vue-highlightjs';

import App from './App';
import router from './router';
import store from './store';
import('../stylus/main.styl');

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));
Vue.config.productionTip = false;

Vue.use(VueHighlightJS);
Vue.use(Vuetify);

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>',
}).$mount('#app');
